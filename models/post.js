'use strict';
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('Post', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    tags: {
      type: DataTypes.STRING,
      allowNull: false
    },
    image: {
      type: DataTypes.STRING,
      allowNull: false
    },
    category: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    user: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    tableName: 'posts'
  });

  /** Связка */
  Model.associate = function(models){
      this.hasMany(models.Comment, {foreignKey: 'post_id'});
  };
  
  /** Формат для веба */
  Model.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return Model;
};