'use strict';
const bcrypt 			= require('bcrypt');
const bcrypt_p 			= require('bcrypt-promise');
const jwt           	= require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
    var Model = sequelize.define('User', {
        first     : DataTypes.STRING,
        last      : DataTypes.STRING,
        email     : {
            type: DataTypes.STRING,
            allowNull: true,
            unique: true,
            validate: {
                isEmail: {msg: "Неверный email адрес"} 
            }},
        phone     : {
            type: DataTypes.STRING,
            allowNull: true,
            unique: true,
            validate: {
                len: {
                    args: [7, 20],
                    msg: "Номер телефона недействителен, слишком короткий."
                },
                isNumeric: {msg: "Введите только число"}
            }
        },
        password  : DataTypes.STRING
    }, {
        tableName: 'users'
    });

    /** Связка */
    Model.associate = function(models){
        this.hasMany(models.Post, {foreignKey: 'user'});
    };

    /** Если пароль изменён, обновим токен */
    Model.beforeSave(async (user, options) => {
        let err;
        if (user.changed('password')){
            let salt, hash
            [err, salt] = await to(bcrypt.genSalt(10));
            if(err) TE(err.message, true);

            [err, hash] = await to(bcrypt.hash(user.password, salt));
            if(err) TE(err.message, true);

            user.password = hash;
        }
    });

    /** Сравнить пароль */
    Model.prototype.comparePassword = async function (pw) {
        let err, pass
        if(!this.password) TE('пароль не установлен');

        [err, pass] = await to(bcrypt_p.compare(pw, this.password));
        if(err) TE(err);

        if(!pass) TE('Неверный пароль');

        return this;
    }
    
    /** Сгенерировать токен и вернуть пользователю */
    Model.prototype.getJWT = function () {
        let expiration_time = parseInt(CONFIG.jwt_expiration);
        return "Bearer "+jwt.sign({user_id:this.id}, CONFIG.jwt_encryption, {expiresIn: expiration_time});
    };

    /** Формат для веба */
    Model.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };

    return Model;
};