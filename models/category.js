'use strict';
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('Category', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  },{
      tableName: 'categories'
  }); 
  
  /** Связка */
  Model.associate = function(models){
      this.hasMany(models.Post, {foreignKey: 'category'});
  };

  /** Формат для веба */
  Model.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return Model;
};