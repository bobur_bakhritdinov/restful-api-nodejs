'use strict';
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('Comment', {
    body: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    post_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  },{
      tableName: 'comments'
  }); 
  
  /** Формат для веба */
  Model.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return Model;
};