const Post = require('../models').Post;
const Category = require('../models').Category;
const User = require('../models').User;

/**
 * Добавить пост
 * @param {*} req 
 * @param {*} res 
 */
const create = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let err, post;
    let user = req.user;
    let post_info = req.body;
    
    post_info.user_id = user.id;
    [err, post] = await to(Post.create(post_info));
    if(err) return ReE(res, err, 422);

    let post_json = post.toWeb();
    return ReS(res,{post:post_json}, 201);
}
module.exports.create = create;

/**
 * Получить список постов
 * @param {*} req 
 * @param {*} res 
 */
const getAll = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let err, posts, category, user, page, pageLimit, sortBy, sortDir;

    //пагинация
    page = (req.query.page) ? parseInt(req.query.page) : null;
    pageLimit = (req.query.limit) ? parseInt(req.query.limit) : null;

    //сортировка
    sortDir = (req.query.sortDir) ? req.query.sortDir : 'DESC';
    sortBy = (req.query.sortBy) ? req.query.sortBy : 'id';

    //посты
    [err, posts] = await to(Post.findAll({
        limit: pageLimit,
        offset: page * pageLimit,
        order: [[sortBy, sortDir]]
    }));
    if(err) return ReE(res, err, 422);

    for(let i in posts) {
        // категория
        [err, category] = await to(Category.findOne({where: {id: posts[i].category}, attributes: ['name']}));
        if(err) return ReE(res, err, 422);
        posts[i].category = category;

        // пользователи
        [err, user] = await to(User.findOne({where: {id: posts[i].user}, attributes: ['first', 'last']}));
        if(err) return ReE(res, err, 422);
        posts[i].user = user;
    }

    return ReS(res, {posts});
}
module.exports.getAll = getAll;

/**
 * Получить по переданному ID
 * @param {*} req 
 * @param {*} res 
 */
const get = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let post, category, user;
    post = req.post;

    // категории
    [err, category] = await to(Category.findOne({where: {id: post.category}, attributes: ['name']}));
    if(err) return ReE(res, err, 422);
    post.category = category;

    // пользователи
    [err, user] = await to(User.findOne({where: {id: post.user}, attributes: ['first', 'last']}));
    if(err) return ReE(res, err, 422);
    post.user = user;

    return ReS(res, {posts: post.toWeb()});
}
module.exports.get = get;

/**
 * Обновить пост по ID
 * @param {*} req 
 * @param {*} res 
 */
const update = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let err, post, data;
    post = req.post;
    data = req.body;

    post.set(data);

    [err, post] = await to(post.save());
    if(err) return ReE(res, err);

    return ReS(res, {post: post.toWeb()});
}
module.exports.update = update;

/**
 * Удалить пост
 * @param {*} req 
 * @param {*} res 
 */
const remove = async function(req, res){
    let post, err;
    post = req.post;

    [err, post] = await to(post.destroy());
    if(err) return ReE(res, err);

    return ReS(res, {success: true}, 204);
}
module.exports.remove = remove;