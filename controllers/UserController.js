/** Библиотеки */
const User          = require('../models').User;
const authService   = require('./../services/AuthService');

/**
 * Добавить пользователя.
 * @param {*} req 
 * @param {*} res
 */
const create = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;

    if(!body.unique_key && !body.email && !body.phone){
        return ReE(res, 'Пожалуйста, введите адрес электронной почты или номер телефона для регистрации.');
    } else if(!body.password){
        return ReE(res, 'Введите пароль для регистрации.');
    }else{
        let err, user;
        [err, user] = await to(authService.createUser(body));

        if(err) return ReE(res, err, 422);
        return ReS(res, {message:'Успешно создан новый пользователь.'}, 201);
    }
}
module.exports.create = create;

/**
 * Авторизация пользователя
 * @param {*} req
 * @param {*} res 
 */
const login = async function(req, res){
    const body = req.body;
    let err, user;

    [err, user] = await to(authService.authUser(req.body));
    if(err) return ReE(res, err, 422);

    return ReS(res, {token:user.getJWT()});
}
module.exports.login = login;