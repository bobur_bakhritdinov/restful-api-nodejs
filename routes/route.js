/** Библиотеки */
const path        = require('path');
const express 	  = require('express');
const router 	  = express.Router();
const passport    = require('passport');

/** Контролеры */
const UserController 	= require('./../controllers/UserController');
const PostController    = require('./../controllers/PostController');

/** Middleware */
const postHandler 	            = require('./../middleware/postHandler').post;
require('./../middleware/passport')(passport);

/** Машруты */
router.post('/user/login',     UserController.login);
router.post('/user/create',    UserController.create);

//посты
router.get('/posts',            PostController.getAll);
router.get('/posts/:postId',    postHandler, PostController.get);
router.post('/posts',           passport.authenticate('jwt', {session:false}), PostController.create);
router.put('/posts/:postId',    passport.authenticate('jwt', {session:false}), postHandler, PostController.update);
router.delete('/posts/:postId', passport.authenticate('jwt', {session:false}), postHandler, PostController.remove);

module.exports = router;
