const Post  = require('./../models').Post;

let post = async function (req, res, next) {
    let postId, err, post;
    postId = req.params.postId;

    [err, post] = await to(Post.findOne({where:{id:postId}}));
    if(err) return ReE(res, err);

    if(!post) return ReE(res, "Пост не найден");
    
    req.post = post;
    next();
}
module.exports.post = post;