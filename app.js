/** Глобальные конфигурации и вспомогательные функции */
require('./config/config');
require('./global_functions');

console.log("Режим:", CONFIG.app)

/** Библиотеки */
const express 		= require('express');
const logger 	    = require('morgan');
const bodyParser 	= require('body-parser');
const passport      = require('passport');
const path          = require('path');

/** Создаем экземпляр экспресс */
const app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/** Пароль */
app.use(passport.initialize());

/** База данных */
const models = require("./models");
models.sequelize.authenticate().then(() => {
    console.log('Соединение успешно установлено!');
})
.catch(err => {
    console.error('Не удалось подключиться к базе данных :',CONFIG.db_name, err);
});
if(CONFIG.app==='dev'){
    models.sequelize.sync();// создать таблицы, если в базе отсувствует.
    // models.sequelize.sync({ force: true }); // удалить все и заново создать таблицы
}

/** Middleware для использования CORS  */
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization, Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

/** Роутеры */
const route = require('./routes/route');
app.use("/public", express.static(path.resolve(__dirname, 'public')));
app.use('/api', route);
app.use('/', function(req, res, next) {
    res.status(403).send('Ошибка 403: Доступ запрещен!');
});

app.use(function(req, res, next) {
  var err = new Error('Ошибка 404: страница не найдена');
  err.status = 404;
  next(err);
});

/** Обработчик ошибок */
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;