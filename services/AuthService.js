/** Библиотеки */
const User 			= require('./../models').User;
const validator     = require('validator');

/**
 * Получить unique_key [метод авторизации]
 * @param {*} body 
 */
const getUniqueKeyFromBody = function(body){
    let unique_key = body.unique_key;
    if(typeof unique_key==='undefined'){
        if(typeof body.email != 'undefined'){
            unique_key = body.email
        }else if(typeof body.phone != 'undefined'){
            unique_key = body.phone
        }else{
            unique_key = null;
        }
    }

    return unique_key;
}
module.exports.getUniqueKeyFromBody = getUniqueKeyFromBody;

/**
 * Добавить пользователя
 * @param {*} userInfo 
 */
const createUser = async function(userInfo){
    let unique_key, auth_info, err;

    auth_info={}
    auth_info.status='create';

    unique_key = getUniqueKeyFromBody(userInfo);
    if(!unique_key) TE('Номер электронной почты или номер телефона не был введен.');

    if(validator.isEmail(unique_key)){
        auth_info.method = 'email';
        userInfo.email = unique_key;

        [err, user] = await to(User.create(userInfo));
        if(err) TE('пользователь уже существует с этим адресом электронной почты');

        return user;

    }else if(validator.isMobilePhone(unique_key, 'any')){
        auth_info.method = 'phone';
        userInfo.phone = unique_key;

        [err, user] = await to(User.create(userInfo));
        if(err) TE('пользователь уже существует с этим номером телефона');

        return user;
    }else{
        TE('Действительный адрес электронной почты или номер телефона не был введен.');
    }
}
module.exports.createUser = createUser;

/**
 * Авторизация пользователя
 * @param {*} userInfo 
 */
const authUser = async function(userInfo){
    let unique_key;
    let auth_info = {};
    auth_info.status = 'login';
    unique_key = getUniqueKeyFromBody(userInfo);

    if(!unique_key) TE('Введите адрес электронной почты или номер телефона для входа');
    if(!userInfo.password) TE('Введите пароль для входа');

    let user;
    if(validator.isEmail(unique_key)){
        auth_info.method='email';

        [err, user] = await to(User.findOne({where:{email:unique_key}}));
        if(err) TE(err.message);

    }else if(validator.isMobilePhone(unique_key, 'any')){
        auth_info.method='phone';

        [err, user] = await to(User.findOne({where:{phone:unique_key }}));
        if(err) TE(err.message);

    }else{
        TE('Действительный адрес электронной почты или номер телефона не был введен');
    }

    if(!user) TE('Не зарегистрирован');

    [err, user] = await to(user.comparePassword(userInfo.password));

    if(err) TE(err.message);

    return user;

}
module.exports.authUser = authUser;